FROM ubuntu:18.10
COPY ["./debs", "/debs"]
RUN \
apt update && \
apt install -y fontforge-nox git libsaxonb-java libxml2-utils make npm openjdk-8-jre-headless /debs/*.deb && \
sed -i 's/PS1=.*/PS1="\\h:\\w\\$ "/g' "/etc/bash.bashrc"
RUN npm install -g csslint htmllint-cli
WORKDIR "/build"
CMD ["/bin/bash"]
