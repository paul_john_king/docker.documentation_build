REGISTRY ::= "registry.gitlab.com/paul_john_king"
NAME ::= "docker.documentation_build"
TAG ::= $(shell\
	set -e;\
	set -u;\
	description="$$(git describe --long --dirty)";\
	major_minor="$${description%%-*}";\
	description="$${description\#$${major_minor}-}";\
	patch="$${description%%-*}";\
	description="$${description\#$${patch}-g}";\
	commit_id="$${description%%-*}";\
	description="$${description\#$${commit_id}}";\
	coda="$${description:+.$${description\#-}}";\
	echo "$${major_minor}.$${patch}_$${commit_id}$${coda}";\
)
ALIAS ::= "${REGISTRY}/${NAME}:${TAG}"

list_images = $(shell docker image ls --format "{{.Repository}}:{{.Tag}}" "$(1)")

define create_image =
	@docker build --tag="$(1)" .
endef

define deploy_image =
	$(if $(call list_images,$(ALIAS)),@docker push "$(1)")
endef

define delete_image =
	$(if $(call list_images,$(ALIAS)),@docker image rm "$(1)")
endef

.PHONY: all create deploy delete help

all: create deploy

create:
	$(call create_image,$(ALIAS))

deploy:
	$(call deploy_image,$(ALIAS))

delete:
	$(call delete_image,$(ALIAS))

help:
	@echo "Usage:"
	@echo "  make all    -- create a local Docker image and deploy it to a remote registry"
	@echo "  make create -- create a local Docker image"
	@echo "  make deploy -- deploy a local Docker image to a remote registry"
	@echo "  make delete -- delete the local Docker image"
	@echo "  make help   -- list and describe the \`make\` targets"
